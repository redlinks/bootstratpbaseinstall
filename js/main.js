


$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        loop:true,
    margin:30,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false,
            loop:true
        },
        1000:{
            items:3,
            nav:true,
            loop:true
        }
    }
    });
});

/* TOP BUTTON */

var btn = $('#gotopbutton');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});



/* MENU CLASS CANGE COLORS */

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 300) {
        //clearHeader, not clearheader - caps H
        $(".mainMenu").addClass("floatingMenu");
    }
    else{
        $(".mainMenu").removeClass("floatingMenu");
    }
}); //missing );



  /* STOP MODAL */

  $(document).ready(function() {
    $('#youtubeVideo').on('hidden.bs.modal', function() {
      var $this = $(this).find('iframe'),
        tempSrc = $this.attr('src');
      $this.attr('src', "");
      $this.attr('src', tempSrc);
    });
  
    $('#html5Video').on('hidden.bs.modal', function() {
      var html5Video = document.getElementById("htmlVideo");
      if (html5Video != null) {
        html5Video.pause();
        html5Video.currentTime = 0;
      }
    });
  });